<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220226152243 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `character` (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, campaign_id INT NOT NULL, name VARCHAR(255) NOT NULL, class VARCHAR(255) NOT NULL, ancestry VARCHAR(255) NOT NULL, background VARCHAR(255) NOT NULL, level INT NOT NULL, perception INT NOT NULL, stealth INT NOT NULL, class_dc INT NOT NULL, ac INT NOT NULL, hp INT NOT NULL, fort INT NOT NULL, ref INT NOT NULL, will INT NOT NULL, notes LONGTEXT DEFAULT NULL, INDEX IDX_937AB03499E6F5DF (player_id), INDEX IDX_937AB034F639F774 (campaign_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `character` ADD CONSTRAINT FK_937AB03499E6F5DF FOREIGN KEY (player_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE `character` ADD CONSTRAINT FK_937AB034F639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE `character`');
    }
}
