<?php

namespace App\Entity;

use App\Repository\CharacterRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CharacterRepository::class)]
#[ORM\Table(name: '`character`')]
class Character
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'characters')]
    #[ORM\JoinColumn(nullable: false)]
    private $player;

    #[ORM\ManyToOne(targetEntity: Campaign::class, inversedBy: 'characters')]
    #[ORM\JoinColumn(nullable: false)]
    private $campaign;

    #[ORM\Column(type: 'string', length: 255)]
    private $class;

    #[ORM\Column(type: 'string', length: 255)]
    private $ancestry;

    #[ORM\Column(type: 'string', length: 255)]
    private $background;

    #[ORM\Column(type: 'integer')]
    private $level;

    #[ORM\Column(type: 'integer')]
    private $perception;

    #[ORM\Column(type: 'integer')]
    private $stealth;

    #[ORM\Column(type: 'integer')]
    private $classDC;

    #[ORM\Column(type: 'integer')]
    private $ac;

    #[ORM\Column(type: 'integer')]
    private $hp;

    #[ORM\Column(type: 'integer')]
    private $fort;

    #[ORM\Column(type: 'integer')]
    private $ref;

    #[ORM\Column(type: 'integer')]
    private $will;

    #[ORM\Column(type: 'text', nullable: true)]
    private $notes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPlayer(): ?User
    {
        return $this->player;
    }

    public function setPlayer(?User $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getAncestry(): ?string
    {
        return $this->ancestry;
    }

    public function setAncestry(string $ancestry): self
    {
        $this->ancestry = $ancestry;

        return $this;
    }

    public function getBackground(): ?string
    {
        return $this->background;
    }

    public function setBackground(string $background): self
    {
        $this->background = $background;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getPerception(): ?int
    {
        return $this->perception;
    }

    public function setPerception(int $perception): self
    {
        $this->perception = $perception;

        return $this;
    }

    public function getStealth(): ?int
    {
        return $this->stealth;
    }

    public function setStealth(int $stealth): self
    {
        $this->stealth = $stealth;

        return $this;
    }

    public function getClassDC(): ?int
    {
        return $this->classDC;
    }

    public function setClassDC(int $classDC): self
    {
        $this->classDC = $classDC;

        return $this;
    }

    public function getAc(): ?int
    {
        return $this->ac;
    }

    public function setAc(int $ac): self
    {
        $this->ac = $ac;

        return $this;
    }

    public function getHp(): ?int
    {
        return $this->hp;
    }

    public function setHp(int $hp): self
    {
        $this->hp = $hp;

        return $this;
    }

    public function getFort(): ?int
    {
        return $this->fort;
    }

    public function setFort(int $fort): self
    {
        $this->fort = $fort;

        return $this;
    }

    public function getRef(): ?int
    {
        return $this->ref;
    }

    public function setRef(int $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getWill(): ?int
    {
        return $this->will;
    }

    public function setWill(int $will): self
    {
        $this->will = $will;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
