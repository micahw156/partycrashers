<?php

namespace App\Controller\Admin;

use App\Entity\Character;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CharacterCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Character::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->add(Crud::PAGE_EDIT, Action::DETAIL);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Character Name'),
            AssociationField::new('player'),
            AssociationField::new('campaign'),
            TextField::new('ancestry', 'Ancestry / Heritage'),
            TextField::new('background'),
            TextField::new('class', 'Class / Archetype'),
            NumberField::new('level'),
            NumberField::new('perception')
                ->hideOnIndex(),
            NumberField::new('stealth')
                ->hideOnIndex(),
            NumberField::new('classDC', 'Class DC')
                ->hideOnIndex(),
            NumberField::new('ac', 'Armor Class')
                ->hideOnIndex(),
            NumberField::new('hp', 'Max Hit Points')
                ->hideOnIndex(),
            NumberField::new('fort', 'Fortitude')
                ->hideOnIndex(),
            NumberField::new('ref', 'Reflex')
                ->hideOnIndex(),
            NumberField::new('will', 'Will')
                ->hideOnIndex(),
            TextEditorField::new('notes')
                ->hideOnIndex()
                ->setTemplatePath('admin/field/texteditor.html.twig'),
        ];
    }

}
