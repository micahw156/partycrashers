<?php

namespace App\Controller\Admin;

use App\Entity\Campaign;
use App\Entity\Character;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Party Crashers');
    }

    public function configureMenuItems(): iterable
    {
        // Find icons at https://fontawesome.com/v5/search?m=free&s=solid
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Users', 'fas fa-user', User::class)
            ->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Campaigns', 'fas fa-dungeon', Campaign::class);
        yield MenuItem::linkToCrud('Characters', 'fas fa-dice-d20', Character::class);
        yield MenuItem::linkToRoute('Return to site', 'fas fa-arrow-alt-circle-left', 'frontpage');
    }

    public function configureCrud(): Crud
    {
        $crud = parent::configureCrud();
        return $crud
            ->showEntityActionsInlined();
    }

}
