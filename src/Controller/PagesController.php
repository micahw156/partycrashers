<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{

    # The site name.
    const SITE_NAME = 'Party Crashers';


    #[Route('/', name: 'frontpage')]
    public function index(): Response
    {
        return $this->render('markdown.html.twig', [
            'site_name' => self::SITE_NAME,
            'title' => 'Welcome',
            'content' => 'Welcome party crashers! _Roll for initiative._',
        ]);
    }

}
