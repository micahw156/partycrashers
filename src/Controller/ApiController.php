<?php

namespace App\Controller;

use App\Entity\User;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{

    #[Route('/api/login', name: 'api_login')]
    public function index(#[CurrentUser] ?User $user): Response
    {
        $user = $this->getUser() ?? NULL;

        if (null === $user) {
             return $this->json([
                 'error' => 'Login failed. Missing user credentials.',
             ], Response::HTTP_UNAUTHORIZED);
        }

        $key = $_ENV["JWT_SECRET"];
        $payload = [
            'sub' => $user->getId(),
            'iat' => time(),
            'exp' => time() + 3600,
        ];
        $token = JWT::encode($payload, $key, 'HS256');

        return $this->json([
            'message' => 'Login successful.',
            'id' => $user->getId(),
            'user' => $user->getUserIdentifier(),
            'email' => $user->getEmail(),
            'roles' => $user->getRoles(),
            'token' => $token,
        ]);
    }
}
